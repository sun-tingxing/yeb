package com.yeb.mail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * @author Suntingxing
 * @date 2021/11/7 23:58
 */
public class Test1111 {
    @Autowired
    private SpringTemplateEngine springTemplateEngine;

    @org.junit.Test
    public void Test1(){
        Context context = new Context();
        String mail = springTemplateEngine.process("mail", context);
        System.out.println(mail);
    }
}
